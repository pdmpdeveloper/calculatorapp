/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('CalculatorApp', [])
        .controller('CalculatorController', function ($scope, $http) {
            $scope.result = function () {

                var ur = "/CalculatorApp/rest/CalulatorService/calculate?a=" + $scope.a + "&b=" + $scope.b + "&operation=";

                if ($scope.operator == '+') {
                    ur += "add";
                }
                if ($scope.operator == '-') {
                    ur += "sub";
                }
                if ($scope.operator == '*') {
                    ur += "mul";
                }
                if ($scope.operator == '/') {
                    ur += "div";
                }

                var c = encodeURI(ur);

                $http.get(c).then(function (response) {
                    $scope.c = response.data.result;
                    $scope.aR = response.data.input1;
                    $scope.bR = response.data.input2;
                    $scope.operatorR = $scope.operator;
                });

            };
        });
 