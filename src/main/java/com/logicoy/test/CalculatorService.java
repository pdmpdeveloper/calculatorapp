/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logicoy.test;

/**
 *
 * @author logicoy
 */

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import javax.ws.rs.QueryParam;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/CalulatorService")
public class CalculatorService {

    private static Logger logger = Logger.getLogger(CalculatorService.class.getCanonicalName());

    public CalculatorService() {
        

    }


    @RequestMapping(value = "/calculate", method = RequestMethod.GET, headers = "Accept=application/json", produces = {"application/json"})
    @ResponseBody
    public Map<String,String> getBPELVariableList(@QueryParam("a") String a, @QueryParam("b") String b,@QueryParam("operation") String operation) {
        Map<String,String> map = new HashMap<>();
        map.put("input1", a);
        map.put("input2", b);
        double d = 0;
        Logger.getLogger("CalculatorService").info("a " + a);
        Logger.getLogger("CalculatorService").info("b " + b);
        Logger.getLogger("CalculatorService").info("Operation " + operation);
        if( operation.trim().equalsIgnoreCase("add")){
            d = Double.parseDouble(a) + Double.parseDouble(b);
            
        } else if( operation.trim().equalsIgnoreCase("sub")){
            d = Double.parseDouble(a) - Double.parseDouble(b);
        } else if( operation.trim().equalsIgnoreCase("mul")){
            d = Double.parseDouble(a) * Double.parseDouble(b);
        } else if( operation.trim().equalsIgnoreCase("div")){
            d = Double.parseDouble(a) / Double.parseDouble(b);
        }
        Logger.getLogger("CalculatorService").info("d " + d);
        map.put("result", String.valueOf(d));
        return map;
    }

   
}
